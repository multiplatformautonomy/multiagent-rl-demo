#include <pytorch-demo/plugins/sensor/CTFSensor/CTFSensor.h>

#include <scrimmage/math/State.h>
#include <scrimmage/common/Time.h>
#include <scrimmage/plugin_manager/RegisterPlugin.h>
#include <scrimmage/plugins/interaction/Boundary/Boundary.h>
#include <scrimmage/pubsub/Subscriber.h>
#include <map>
#include <string>
#include <scrimmage/msgs/Capture.pb.h>
#include <scrimmage/parse/ParseUtils.h>

using std::cout;
using std::endl;

namespace sc = scrimmage;
namespace sm = scrimmage_msgs;
namespace sci = scrimmage::interaction;

REGISTER_PLUGIN(scrimmage::Sensor,
                scrimmage::sensor::CTFSensor,
                CTFSensor_plugin)

namespace scrimmage {
namespace sensor {
    void CTFSensor::init(std::map<std::string, std::string> &params) {
        capture_boundary_id_ = get<int>("capture_boundary_id", params, capture_boundary_id_);
        flag_boundary_id_ = get<int>("flag_boundary_id", params, flag_boundary_id_);

        auto flag_taken_cb = [&] (scrimmage::MessagePtr<sm::FlagTaken> msg) {
            if (msg->data.entity_id() == parent_->id().id() &&
                msg->data.flag_boundary_id() == flag_boundary_id_) {
                has_flag_ = true;
            }
        };
        subscribe<sm::FlagTaken>("GlobalNetwork", "FlagTaken", flag_taken_cb);

        auto boundary_cb = [&] (scrimmage::MessagePtr<sp::Shape> msg) {
            std::shared_ptr<sci::BoundaryBase> boundary = sci::Boundary::make_boundary(msg->data);
            boundaries_[msg->data.id().id()] = std::make_pair(msg->data, boundary);   
        };
        subscribe<sp::Shape>("GlobalNetwork", "Boundary", boundary_cb);
    }

    void CTFSensor::find_closest_enemy(){
        double min_dist = std::numeric_limits<double>::infinity();
        for (auto &kv : *(parent_->contacts())) {
            sc::Contact &cnt = kv.second;
            if (cnt.id().team_id() == parent_->id().team_id()) {
                continue; // Ignore same team
            }
            if (closest_ == nullptr || (parent_->state()->pos() - closest_->pos()).norm() < min_dist) {
                closest_ = cnt.state();
                min_dist = (parent_->state()->pos() - closest_->pos()).norm();
            }
        }
    }

    void CTFSensor::get_observation(int* data, uint32_t beg_idx, uint32_t end_idx) {
        if (parent_ == NULL){
            std::cout << "*******  AGENT REMOVED ****** " << std::endl;
            return;
        } 
        uint32_t beginning = beg_idx;
        auto it = boundaries_.find(capture_boundary_id_);
        bool in_base = it != boundaries_.end() && std::get<1>(it->second)->contains(parent_->state()->pos()); 
        Eigen::Vector3d obj;
        it = has_flag_ ? boundaries_.find(capture_boundary_id_) : boundaries_.find(flag_boundary_id_);
        if (it != boundaries_.end()) {
            obj = std::get<1>(it->second)->center();
        }
        closest_ = nullptr;
        if (in_base){ //if agent not in base, shouldn't go after enemy
            find_closest_enemy();
            if (closest_!=nullptr){
                obj = closest_->pos();
                bool enemy_in_base = std::get<1>(it->second)->contains(obj);  
                if (!enemy_in_base)
                    closest_ = nullptr;
            }         
        }
        
        Eigen::Vector3d to_obj = parent_->state()->pos() - obj;
        double heading = atan2(to_obj[0], to_obj[1]);
        heading = heading < 0.0 ? (3.14592*2-abs(heading)) : heading;
        int heading_bin = std::min(int( (heading/(3.14592*2))*8 ), 7);
        data[beginning++] = heading_bin;
    }

	void CTFSensor::set_observation_space() {
        observation_space.discrete_count.push_back(8); //heading to obj
	}
} // namespace sensor
} // namespace scrimmage

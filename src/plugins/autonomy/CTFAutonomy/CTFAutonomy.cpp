/*!
 * @file
 *
 * @section LICENSE
 *
 * Copyright (C) 2017 by the Georgia Tech Research Institute (GTRI)
 *
 * This file is part of SCRIMMAGE.
 *
 *   SCRIMMAGE is free software: you can redistribute it and/or modify it under
 *   the terms of the GNU Lesser General Public License as published by the
 *   Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *   SCRIMMAGE is distributed in the hope that it will be useful, but WITHOUT
 *   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *   License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SCRIMMAGE.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin DeMarco <kevin.demarco@gtri.gatech.edu>
 * @author Eric Squires <eric.squires@gtri.gatech.edu>
 * @date 31 July 2017
 * @version 0.1.0
 * @brief Brief file description.
 * @section DESCRIPTION
 * A Long description goes here.
 *
 */

#include <pytorch-demo/plugins/autonomy/CTFAutonomy/CTFAutonomy.h>

#include <scrimmage/plugin_manager/RegisterPlugin.h>
#include <scrimmage/entity/Entity.h>
#include <scrimmage/math/State.h>
#include <scrimmage/parse/ParseUtils.h>
#include <scrimmage/pubsub/Subscriber.h>
#include <scrimmage/plugins/interaction/Boundary/Boundary.h>
#include <scrimmage/common/Waypoint.h>
#include <scrimmage/plugins/autonomy/WaypointGenerator/WaypointList.h>
#include <scrimmage/msgs/Capture.pb.h>

using std::cout;
using std::endl;

namespace sc = scrimmage;
namespace sm = scrimmage_msgs;
namespace sci = scrimmage::interaction;

REGISTER_PLUGIN(scrimmage::Autonomy,
                scrimmage::autonomy::CTFAutonomy,
                CTFAutonomy_plugin)

namespace scrimmage {
namespace autonomy {

  void CTFAutonomy::init_helper(std::map<std::string, std::string> &params) {

    output_vel_x_idx_ = vars_.declare(VariableIO::Type::velocity_x, VariableIO::Direction::Out);
    output_vel_y_idx_ = vars_.declare(VariableIO::Type::velocity_y, VariableIO::Direction::Out);
    output_vel_z_idx_ = vars_.declare(VariableIO::Type::velocity_z, VariableIO::Direction::Out);

    flag_boundary_id_ = get<int>("flag_boundary_id", params, flag_boundary_id_);
    capture_boundary_id_ = get<int>("capture_boundary_id", params, capture_boundary_id_);
    max_speed_ = get<int>("max_speed", params, max_speed_);
    reward_ = get<int>("reward", params, reward_);

    action_options_[0] = std::make_pair(0, max_speed_); 
    action_options_[1] = std::make_pair(0,-max_speed_); 
    action_options_[2] = std::make_pair(-max_speed_,0); 
    action_options_[3] = std::make_pair(max_speed_,0);
    action_options_[4] = std::make_pair(max_speed_, max_speed_); 
    action_options_[5] = std::make_pair(-max_speed_,-max_speed_); 
    action_options_[6] = std::make_pair(-max_speed_,max_speed_); 
    action_options_[7] = std::make_pair(max_speed_,-max_speed_);

    auto callback = [&] (scrimmage::MessagePtr<sp::Shape> msg) {
        std::shared_ptr<sci::BoundaryBase> boundary = sci::Boundary::make_boundary(msg->data);
        boundaries_[msg->data.id().id()] = std::make_pair(msg->data, boundary);
    };
    subscribe<sp::Shape>("GlobalNetwork", "Boundary", callback);

    auto flag_taken_cb = [&] (scrimmage::MessagePtr<sm::FlagTaken> msg) {
        if (msg->data.entity_id() == parent_->id().id() &&
            msg->data.flag_boundary_id() == flag_boundary_id_) {
            has_flag_ = true;
        }
    };
    subscribe<sm::FlagTaken>("GlobalNetwork", "FlagTaken", flag_taken_cb);

    auto flag_captured_cb = [&] (scrimmage::MessagePtr<sm::FlagCaptured> msg) {
        if (msg->data.entity_id() == parent_->id().id()) {
            flag_captured_ = true;
            // std::cout << "! --- CTFAutonomy cb - flag_captured_. "<< std::endl;
        }
        else{
          enemy_won_ = true;
        }
    };
    subscribe<sm::FlagCaptured>("GlobalNetwork", "FlagCaptured", flag_captured_cb);

    auto nonteam_capture_cb = [&] (scrimmage::MessagePtr<sm::NonTeamCapture> msg) {
        if (msg->data.target_id() == parent_->id().id()) {
            enemy_won_ = true; 
        } else {
            enemy_captured_id_ = msg->data.target_id();
            // std::cout << "! --- CTFAutonomy cb - enemy captured. "<< std::endl;
        }
    };
    subscribe<sm::NonTeamCapture>("GlobalNetwork", "NonTeamCapture", nonteam_capture_cb);
  }

  void CTFAutonomy::set_environment() {
      // Note: a default reward range set to [-inf,+inf] already exists. Set it if you want a narrower range.
      // reward_range = std::make_pair(0, 2);
    action_space.discrete_count.push_back(8);
  }

  void CTFAutonomy::find_closest_enemy(){
    double min_dist = std::numeric_limits<double>::infinity();
    for (auto &kv : *contacts_) { 
      sc::Contact &cnt = kv.second;
      if (cnt.id().team_id() == parent_->id().team_id()) {
          continue; // Ignore same team
      }
      if (closest_ == nullptr || (state_->pos() - closest_->pos()).norm() < min_dist) {
          closest_ = cnt.state();
          min_dist = (state_->pos() - closest_->pos()).norm();
      }
    }
  }

  std::tuple<bool, double, pybind11::dict> CTFAutonomy::calc_reward() {
    // pybind11::list obs = observations_.observation.cast<pybind11::list>();
      double reward = 0;
      pybind11::dict info;
      // check if entity has been removed from simulation - captured, collision, oob
      if (state_ == NULL || enemy_won_) {
        if (state_ == NULL)
          reward = -reward_;
        return std::make_tuple(true, reward, info);
      }

      auto it = boundaries_.find(capture_boundary_id_);
      bool in_base = it != boundaries_.end() && std::get<1>(it->second)->contains(parent_->state()->pos()); 
      Eigen::Vector3d obj;
      closest_ = nullptr;
      if (in_base){ 
        find_closest_enemy();
        if (closest_!=nullptr){
            obj = closest_->pos();
            bool enemy_in_base = std::get<1>(it->second)->contains(obj);  
            if (!enemy_in_base)
              closest_ = nullptr;
        }         
      }
      if (closest_==nullptr){
        it = has_flag_ ? boundaries_.find(capture_boundary_id_) : boundaries_.find(flag_boundary_id_);
          if (it != boundaries_.end()) {
            obj = std::get<1>(it->second)->center();
          } 
      }
      double obj_dist = (state_->pos() - obj).norm();
      
      //Reward
      int flag_taken_reward = has_flag_ && !prev_has_flag_ ? reward_ : 0;   //reward for taking the flag
      int flag_captured_reward = flag_captured_ ? reward_ : 0; //reward for bringing flag back to base
      int enemy_captured_reward = enemy_captured_id_ > 0 ? reward_ : 0;  // reward for capturing enemy
      bool use_dist = true;
      if (prev_obj_dist_ > max_dist_ || obj_dist > max_dist_){
        use_dist = false;
        // std::cout << "! -- ERROR - DIST TOO BIG" << std::endl;
      }
      double closer_to_obj = use_dist ? (prev_obj_dist_ - obj_dist) : 0;
      closer_to_obj = (abs(closer_to_obj) > obj_switch_dist_) ? 0 : closer_to_obj;
      reward =  flag_taken_reward + flag_captured_reward + enemy_captured_reward + closer_to_obj;
    
      prev_has_flag_ = has_flag_;
      enemy_captured_id_ = -1; 
      prev_obj_dist_ = obj_dist;

      info["info"] = obj_dist; // debugging information
      return std::make_tuple(flag_captured_, reward, info);
  }

  bool CTFAutonomy::step_helper() {
      const double x_vel = action_options_[action.discrete[0]].first;
      const double y_vel = action_options_[action.discrete[0]].second;

      vars_.output(output_vel_x_idx_, x_vel);
      vars_.output(output_vel_y_idx_, y_vel);
	  return true;
  }

} // namespace autonomy
} // namespace scrimmage

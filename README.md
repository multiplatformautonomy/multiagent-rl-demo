# OpenAI Gym Multi-Agent Reinforcement Learning Autonomy Demo
Based off of https://gitlab.com/multiplatformautonomy/ctf-rl-demo

### Build

These instructions assume that you have already cloned and built SCRIMMAGE. More information can be found at https://github.com/gtri/scrimmage.

After cloning this repo, create a build folder within the repo folder and build the CTFAutonomy and CTFSensor plugins. Source `~/.scrimmage/setup.bash`.

```
mkdir build && cd build
cmake ..
make
source ~/.scrimmage/setup.bash
```


### Run

The main function for the multi-agent demo is found in `ctf-qlearn-2blue.py`. To turn training mode on or off, change the value of the `train_mode` variable in the `test_openai` function call in the top-level code at the end of the file. Setting this variable to `True` will run the demo in training mode, while setting the variable to `False` will run a single mission using the saved Q-learner model. 

To run the demo, simply run `ctf-qlearn-2blue.py` as a script.

```
python3 ctf-qlearn-2blue.py
```

### Current Demo Status

The `master` branch contains a version of the demo that uses a single Q-learner to control two agents on team 1 flying against one agent on team 2 that uses the TakeFlag autonomy plugin in a game of Capture the Flag. The Q-learner in this version is able to train a model sufficient enough to win the game. 

The `independent-qlearners` branch contains a version of the demo that assigns independent Q-learners to each of two agents on team 1 flying against one agent on team 2 that uses the TakeFlag autonomy plugin. While this version runs without issue, the models created by the individual Q-learners is not currently sufficient enough to defeat the single non-learning agent. 